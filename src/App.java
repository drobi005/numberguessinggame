import java.util.Random;
import java.util.Scanner;

public class App{
	public static void main(String[] args) {
		Random r = new Random();
		final int MAX=101;
		int NUMBER;
		int guess,guesses;
		boolean playGame;
		String playAgain = " ";
		Scanner in = new Scanner(System.in);
		
		do
		{
			guesses=0;	//initialize guess attempts to 0
			NUMBER=r.nextInt(MAX); //generate a new number between [0-100]
			playGame = true;
			
			while(playGame)
			{
				System.out.println("Pick a number!"); //prompt user to enter number
				guess = in.nextInt();
				guesses++;//increment guess attempts
				//compare guess with actual number
				if (guess == NUMBER)
				{
					System.out.println("You win!");
					System.out.println("Number of guesses: "+guesses);
					playGame=false;
				}
				if (guess < NUMBER){
					System.out.println("Nope! The number is greater than "+guess);
				}
				if (guess > NUMBER){
					System.out.println("Nope! The number is less than "+guess);
				}
			}
			
			System.out.println("Do you want to play again? Enter (Y)es or (N)o:");
			playAgain = in.next();
		}while(playAgain.equalsIgnoreCase("y") || playAgain.equalsIgnoreCase("Y"));
		System.out.println("Thank you for playing!");
		in.close();
	}
}